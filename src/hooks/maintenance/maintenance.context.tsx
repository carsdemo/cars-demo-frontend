import { IApiMaintenance } from 'data/interfaces';
import React from 'react';

const MaintenanceContext = React.createContext({
    avatar: '',
    cars: [],
    show: false,
    createMaintenance: (data: IApiMaintenance) => {},
    showModal: () => {},
    hideModal: () => {}
});

export default MaintenanceContext;