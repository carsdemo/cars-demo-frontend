import React, { useEffect, useReducer } from 'react';
import { IMAGES_ROUTES } from '../../data/constants';
import { MaintenanceReducer } from './reducer/maintenance.reducer';
import MaintenanceContext from './maintenance.context';
import axios from 'axios';
import { API_ROUTES } from 'data/routes/api.routes';
import { MAINTENANCE_TYPES } from './types/maintenance.types';
import { IApiMaintenance } from 'data/interfaces';

const MaintenanceProvider = (props: any) => {
    const {children} = props;

    const hideModal = () => {
        setState({type: MAINTENANCE_TYPES.HIDE_MODAL, payload: false});
    }

    const showModal = () => {
        setState({type: MAINTENANCE_TYPES.SHOW_MODAL, payload: true});
    }

    const fetchCars = () => {
        axios.get(API_ROUTES.CARS.LIST)
            .then((r) => {
                setState({type: MAINTENANCE_TYPES.GET_ALL_CARS, payload: r.data});
            }).catch(() => {})
    }

    const createMaintenance = (data: IApiMaintenance) => {
        axios.put(API_ROUTES.CARS.NEW_MAINTENANCE, data)
            .then((r) => {
                setState({type: MAINTENANCE_TYPES.NEW_MAINTENCE, payload: r.data})
                hideModal();
            }).catch((e) => {})
    }

    const initialState = {
        avatar: IMAGES_ROUTES.USER_DEFAULT,
        cars: [],
        show: false,
        createMaintenance,
        hideModal,
        showModal
    };

    useEffect(() => {
        fetchCars();
    }, [])

    const [state, setState] = useReducer(MaintenanceReducer, initialState);

    return (
        <MaintenanceContext.Provider value={state}>
            {children}
        </MaintenanceContext.Provider>
    )
}

export default MaintenanceProvider;