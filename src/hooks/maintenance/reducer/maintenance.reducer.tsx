import { ICardMaintenance } from 'data/interfaces';
import { MAINTENANCE_TYPES } from '../types/maintenance.types';

export function MaintenanceReducer(state: any, action: {type: any; payload: any;}) {
    const currentState = {...state};
    switch (action.type) {
        case MAINTENANCE_TYPES.GET_ALL_CARS:
            currentState.cars = action.payload;
            return currentState;
        case MAINTENANCE_TYPES.NEW_MAINTENCE:
            currentState.cars.map((c: ICardMaintenance) => {
                if (c.id === action.payload.id) {
                    c.estimatedDate = action.payload.estimatedDate;
                    c.person = action.payload.person;
                }
                return c;
            });
            return currentState;
        case MAINTENANCE_TYPES.HIDE_MODAL:
            currentState.show = false;
            return currentState;
        case MAINTENANCE_TYPES.SHOW_MODAL:
            currentState.show = true;
            return currentState;
        default:
            return currentState;
    }
}