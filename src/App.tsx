import React from 'react';
import './assets/scss/styles.scss'
import MaintenanceProvider from './hooks/maintenance/maintenance.provider';
import { HomeView } from './modules/home/Home.view';
import { SkeletonComponent } from './components';

export const App: React.FC = () =>  {
  return (
    <MaintenanceProvider>
      <SkeletonComponent>
        <HomeView/>
      </SkeletonComponent>
    </MaintenanceProvider>
  );
}
