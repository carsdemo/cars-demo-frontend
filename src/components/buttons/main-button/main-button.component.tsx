import React from 'react';
import './main-button.component.scss';

export const MainButtonComponent: React.FC<{
    name: string;
    disabled?: boolean;
    action: () => void;
}>= ({name, disabled = false, action}) => {
    return (
        <button
            className="main-button"
            onClick={action}
            disabled={disabled}
        >{name}</button>
    )
}