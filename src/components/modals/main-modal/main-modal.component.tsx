import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import './main-modal.component.scss';

export const MainModalComponent: React.FC<{
        title: string;
        children?: any;
        show: boolean;
        hideModal: () => void;
    }>= ({title, children, show, hideModal}) => {
        
    return (
        <>
            {
                show && 
                <div className="modal-container">
                    <div className="main-modal">
                        <div className="main-modal__header">
                            <h1>{title}</h1>
                            <FontAwesomeIcon 
                                icon={faTimes}
                                className="main-modal__header-icon"
                                onClick={hideModal}
                            />
                        </div>
                        <div className="main-modal__content">
                            {children}
                        </div>
                    </div>
                </div>
            }
            
        </>     
    )
}