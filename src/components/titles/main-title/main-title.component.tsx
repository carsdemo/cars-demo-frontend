import React from 'react';
import './main-title.component.scss';

export const MainTitleComponent: React.FC<{title: string}>= ({title}) => {
    return (
        <h1 className="main-title">{title}</h1>
    )
}