import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTools } from '@fortawesome/free-solid-svg-icons'
import './card-maintenance.component.scss';
import { ICardMaintenance } from 'data/interfaces';
import { getDate } from 'utils/dates.util';

export const CardMaintenanceComponent: React.FC<{d: ICardMaintenance, action: (d: ICardMaintenance) => void}>= ({d, action}) => {


    const data: Array<{prop: string; val: any, twoLines: boolean}> = [
        {prop: 'ID:', val: d.id, twoLines: false},
        {prop: 'Make:', val: d.make, twoLines: false},
        {prop: 'Model:', val: d.model, twoLines: false},
        {prop: 'Description:', val: d.description, twoLines: true},
        {prop: 'Estimated date:', val: d.estimatedDate ? getDate(d.estimatedDate) : '', twoLines: true},
        {prop: 'KM:', val: d.km + ' km', twoLines: false},
        {prop: 'Person:', val: d.person, twoLines: true}
    ];
    return (
        <div className={
            "card-maintenance "
            + (d.estimatedDate && d.estimatedDate !== null ? 'inmaintenance': '')
        }>
            <div className="card-maintenance__avatar" style={{backgroundImage: 'url('+d.image+')'}}></div>
            <div className="card-maintenance__props">
                {
                    data.map((r, index) => {
                        return (
                            <div className={
                                    "card-maintenance__prop "
                                    + (r.twoLines ? 'two-lines ': '')
                                }
                                key={index}
                            >
                                <h3>{r.prop}</h3>
                                <h3>{r.val}</h3>
                            </div>
                        )
                    })
                }
                {
                    !(d.estimatedDate && d.estimatedDate !== null) &&
                    <div className="card-maintenance__action" onClick={(e) => action(d)}>
                        <FontAwesomeIcon icon={faTools}/>
                    </div>
                }
                
            </div>
            
        </div>
    )
}