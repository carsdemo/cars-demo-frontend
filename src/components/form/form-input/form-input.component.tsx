import React from 'react';
import './form-input.component.scss';

export const FormInputComponent: React.FC<{
    label: string;
    type: 'text' | 'date' | 'datetime-local';
    val: any;
    placeholder?: string;
    change: (e: string) => void
}>= ({label, type, val, change, placeholder = ''}) => {
    return (
        <div className="form-input">
            <label>{label}</label>
            <input 
                type={type} 
                value={val} 
                placeholder={placeholder}
                onChange={(e) => change(e.target.value)}
            />
        </div>
    )
}