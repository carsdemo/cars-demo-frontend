import React from 'react';
import './footer.component.scss';

export const FooterComponent: React.FC = () => {
    return (
        <footer>Developed by José Iván Martínez Larios</footer>
    )
}