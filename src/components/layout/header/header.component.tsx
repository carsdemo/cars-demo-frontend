import React from 'react';
import { IMAGES_ROUTES } from '../../../data/constants';
import './header.component.scss';

export const HeaderComponent: React.FC = () => {
    return (
        <header>
            <h1>ACCompany</h1>
            <div className="avatar" style={{backgroundImage: 'url('+IMAGES_ROUTES.USER_DEFAULT+')'}}></div>
        </header>
    )
}