import React from 'react';
import './maincontent.component.scss';

export const MainContentComponent: React.FC = ({children}) => {
    return (
        <main>{children}</main>
    )
}