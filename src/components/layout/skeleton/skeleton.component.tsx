import React from 'react';
import './skeleton.component.scss';
import { HeaderComponent } from '../header/header.component';
import { MainContentComponent } from '../maincontent/maincontent.component';
import { FooterComponent } from '../footer/footer.component';

export const SkeletonComponent: React.FC = ({children}) => {
    return (
        <section className="layout-container">
            <HeaderComponent/>
            <MainContentComponent>
                {children}
            </MainContentComponent>
            <FooterComponent></FooterComponent>
        </section>
    )
}