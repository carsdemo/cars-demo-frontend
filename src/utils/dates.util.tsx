export const  getDate = (date: string) => {
    const d = new Date(date);
    const day = d.getDate() > 9 ? d.getDate() : `0${d.getDate()}`;
    const month = (d.getMonth() + 1) > 9 ? (d.getMonth() + 1) : `0${d.getMonth() + 1}`;
    const hours = d.getHours() > 9 ? d.getHours() : `0${d.getHours()}`;
    const minutes = d.getMinutes() > 9 ? d.getMinutes() : `0${d.getMinutes()}`;
    return `${d.getFullYear()}/${month}/${day} ${hours}:${minutes}`
}