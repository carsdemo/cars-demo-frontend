import React, { useContext, useState } from 'react';
import { CardMaintenanceComponent, FormInputComponent, MainButtonComponent, MainModalComponent, MainTitleComponent } from 'components';
import './Home.view.scss';
import { IApiMaintenance, ICardMaintenance } from 'data/interfaces';
import MaintenanceContext from 'hooks/maintenance/maintenance.context';

export const HomeView: React.FC = () => {
    const { cars, createMaintenance, show, hideModal, showModal } = useContext(MaintenanceContext)
    // const [show, setShow] = useState(false);
    const [modalData, setModalData] = useState({
        id: 0,
        description: '',
        model: '',
        make: '',
        km: 0,
        estimatedDate: '',
        image: ''
    } as ICardMaintenance);
    // Form
    const [person, setPerson] = useState('');
    const [date, setDate] = useState('');

    // const __handleHideModal = () => setShow(false);
    const __clearForm = () => {
        setPerson('');
        setDate('');
    }

    const __handleSetModalData = (d: ICardMaintenance) => {
        setModalData({...d});
        // setShow(true);
        showModal();
        __clearForm();
    }

    const __handleSaveForm = () => {
        const data: IApiMaintenance = {
            id:modalData.id,
            person,
            date
        };
        createMaintenance(data);
    }


    return (
        <div>
            <MainTitleComponent title="Vehicles"/>
            <div className="hv-container">
                {
                    cars.map((r, index) => {
                        return(
                            <CardMaintenanceComponent 
                                key={index}
                                d={r}
                                action={__handleSetModalData}
                            />
                        )
                    })
                }
            </div>
            <MainModalComponent hideModal={hideModal} show={show} title="Maintenance">
                <div className="m-info">
                    <h1>ID: <span>{modalData.id}</span></h1>
                    <h1>Make: <span>{modalData.make}</span></h1>
                    <h1>Model: <span>{modalData.model}</span></h1>
                    <h1 className="twolines">Description: <span>{modalData.description}</span></h1>
                    <h1>KM: <span>{modalData.km} km</span></h1>
                </div>
                <div className="m-form">
                    <FormInputComponent
                        label="Person*"
                        val={person}
                        type="text"
                        change={(e) => setPerson(e)}
                        placeholder="Ej: John Wilson Walter"
                    />
                    <FormInputComponent
                        label="Estimated date*"
                        val={date}
                        type="datetime-local"
                        change={(e) => setDate(e)}
                    />
                    <MainButtonComponent 
                        name="Add maintenance"
                        action={__handleSaveForm}
                        disabled={date === '' || person === ''}
                    />
                </div>
            </MainModalComponent>
        </div>
    )
}