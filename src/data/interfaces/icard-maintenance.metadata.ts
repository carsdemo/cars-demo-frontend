export interface ICardMaintenance {
    id: number;
    description: string;
    model: string;
    make: string;
    km: number;
    estimatedDate?: string;
    image: string;
    person?: string;
};