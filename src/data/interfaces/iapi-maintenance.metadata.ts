export interface IApiMaintenance {
    id: number;
    person: string;
    date: string;
}